I know it doesn't work:
https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex6/p5/empty-example/index.html

[!Screenshot](hmmm.png)

Yea, so this week I spent an immense amount of time working on this 'game' which ended up not being a game. I used a lot of time on trying to make sprites and classes work together via some sort of collide code, which I just could not get to work. The result is (since I had to make a class) a class that makes some rules for future circle-spawning, and a sprite that was supposed to interact with said class. The idea I had was that I wanted different circles to fall down from above: the green ones were the ones that should be caught with the rectangle sprite at the bottom, while the red circles were supposed to fall and would lead to game over if touched.

I know this could be coded with sprites purely, and would be incredibly easier but I simply do not have the time to do that today. Anyhow, I feel this has led to me knowing more about the restrictions of both the p5 play library and javascript in general. 

In the constructor I have defined the x-position to be random within the canvas' width, the y-position to be 0, the diameter of the circle so be a random number between 10 and 30, and then I tried to give it some sort of collider property which didn't end up working sadly (this I forgot to delete before the final upload).

```
constructor() {
    this.x = random(width);
    this.y = 0;
    this.diameter = random(10, 30);
    this.speed = random(1, 5);
    this.setCollider = 'circle', 0, 0, this.diameter/2;
  }
```

I defined the it's movement to be downward (from the top of the canvas to the bottom), at the speed defined in the constructor. The speed being random between 1 and 5.
```
  move() {
    this.y += this.speed;
  }
```

The display defines the visual aspect of the object. Here I defined the colour to be red (being green i nthe other class in the code) while the shape itself is an ellipse with the coordinates and diameter created above.
  ```
display() {
    push()
    fill(255, 0, 0)
    ellipse(this.x, this.y, this.diameter, this.diameter);
    pop();
  }
```

The possibilities of OOP are the options to "mass-create" objects from one definition instead of having to define each and every single object in the canvas. One thing that is important to notice however, is that the programmer has to either defined all objects created by a class to be completely identical or forces him to make the definitions of different aspects such as the x-position of the width of the object random (or random within certain limits) and thereby can create outcomes not wanted by said programmer.


Thinking in a broader digital context abstraction can be seen in many different places. One of the most obvious areas being social media where lots of complex coding structures and collective methods (i.e. Facebook's data collection on its users) convey simple "readable" outputs to the users. Regarding the data collection Facebook manages to convey the inquiry and necessity of data as something as simple: i.e. the need for access to the camera and microphone so the user can talk via its messenger video call service whilst Facebook 'also' need the access to these functions to be able to listen in on conversations and so on for advertisement purposes.

