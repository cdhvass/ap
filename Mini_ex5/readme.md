Please do give it a little while to load - it took some time om my side - I have also realised that there is a bug that appears when hosting on githack which makes the video not automatically play without the user clicking once on the screen - THIS IS NOT INTENDED THAT WAY:
https://glcdn.githack.com/NicolaiRieder/ap/raw/master/Mini_ex5/p5/empty-example/index.html

One thing that has literally been pissing me off is that none of the host sites seem to be able to load the index.html as it looks on my live server
Statically loaded it correctly besides not being able to load the video:
https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex5/p5/empty-example/index.html

The idea is that the video that appears is also centered and not off-centered as I have seen a couple of times now via the githack link. Another problem has been that githack seems to load the video under the canvas so one could scroll down and see it under the yellow canvas. This is not supposed to happen and does not happen in my live server, the code seems to be correct which leads me to believe that it is indeed a hosting issue.

This week I have spent a lot of time trying to get the video to work and while doing that I encountered a lot of problems regarding how the code should be written to work how it was intended. One of the problems I had included the function added to play and pause the video, as I wanted the video to stop while the mouse button was being held down and pause when said button was released, luckily I found a way for it work. I also ended up finding a way to load both the video, the image and make the "stackable" which I had a great deal of trouble with.

My throbber itself was not intended as any critical comment on anything. The spinning motion of the throbbers simply reminded me of the old Batman intro and the spinning batman logo that was used as a transition in the cartoon for many years.

// UPDATE
I decided to revisit my week 3 assignment which involved a canvas where if pressed the batman intro video would play. The canvas itself consisted of the batman logo spinning (As a throbber) where above some text could be read: "Click and hold for nostalgia". Based on the feedback from last time I decided to remove the 'manual' throbber that was the little yellow circle that could be seen spinning in the opposite direction of the batman logo, as it seemed both distracting and not needed.

This time around the video automatically plays and the batman throbber interrupts the video by 'buffering' it (Not literally buffering, but acts as how a throbebr would appear if a video was buffering). I've included time as an aspect of my code this time to define when the video should pause and the throbber and vice versa.








![Screenshot](batmobile.PNG) ![Screenshot](bm2.png)


