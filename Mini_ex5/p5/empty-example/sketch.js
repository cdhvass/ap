//defining several variables that are needed for the code to work
var batman;
var playing=true;
let img;
var x = 1.00
var zoom = 3.05;
var zMax = 5.00;
var zMin = 1.00;
let font,
  fontsize = 40;
var number = 0;
var lastPrint;
var i = 0;
var timeCounter;




  //below the font is loaded from a .ttf file (a font file type) so that it can be used in the code
  function preload() {
    font = loadFont('assets/batmfa.ttf');
  }

  //here we let "batman" create the video types .mov and .mp4 so it works in several browsers (mac and windows)
  //this is also where the batman logo image is loaded and then afterwards the text definitions are set
function setup() {
  createCanvas(windowWidth, windowHeight);
  batman = createVideo(['assets/batman.mp4', 'assets/batman.mov']);
  batman.hide();
  img = loadImage('assets/spinning.png');
  textFont(font);
 textSize(fontsize);
 textAlign(CENTER, CENTER);
 lastPrint = millis() - 3000;
}

//defines that when the mouse is pressed (and held down) the intro for the old batman cartoon will appear
  //the rotation is defined for the image i.e. the batman logo
  //this defines that if it is not pressed i.e. the mouse is released, then the batman logo and a throbber will appear spinning
function draw() {
  background(0);
  var timeElapsed = millis() - lastPrint;

  if (timeElapsed > 3000) {
    number++;
   i++;
   console.log(i);
   lastPrint = millis();
}

if (number%2==0){
  imageMode(CENTER)
  image(batman, width/2, height/2);
  batman.play();
    playing = true;
} else if (number%2!=0) {
  push();
  translate(width / 2, height / 2);
  background(253,227,17);
  text("Getting the batmobile (buffering)", 0, -300);
  rotate(millis() / -520);
  scale(zoom);
   imageMode(CENTER);
   image(img, 0, 0, 150, 150);
   pop();
   batman.pause();
   playing = false;
}
}
