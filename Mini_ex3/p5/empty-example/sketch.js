//defining several variables that are needed for the code to work
var batman;
var playing=false;
let img;
var x = 1.00
var zoom = 3.05;
var zMax = 5.00;
var zMin = 1.00;
let font,
  fontsize = 40;



  //below the font is loaded from a .ttf file (a font file type) so that it can be used in the code
  function preload() {
    font = loadFont('assets/batmfa.ttf');
  }

  //here we let "batman" create the video types .mov and .mp4 so it works in several browsers (mac and windows)
  //this is also where the batman logo image is loaded and then afterwards the text definitions are set
function setup() {
  createCanvas(windowWidth, windowHeight);
  batman = createVideo(['assets/batmam.mov', 'assets/batman.mp4']);
  batman.hide();
  img = loadImage('assets/spinning.png');
  textFont(font);
 textSize(fontsize);
 textAlign(CENTER, CENTER);

}

//defines that when the mouse is pressed (and held down) the intro for the old batman cartoon will appear
  //the rotation is defined for the image i.e. the batman logo
  //this defines that if it is not pressed i.e. the mouse is released, then the batman logo and a throbber will appear spinning
function draw() {
  background(0);

if (mouseIsPressed){
  imageMode(CENTER)
  image(batman, width/2, height/2);
} else {
  push()
  translate(width / 2, height / 2);
  background(253,227,17)
  text("Click and hold for nostalgia", 0, -300)
  rotate(millis() / -520);
  scale(zoom)
   imageMode(CENTER);
   image(img, 0, 0, 150, 150)
pop()
drawThrobber(100)

}
}

//this is where we define drawThrobber, which draws the circle(s) that spin the opposite direction of the batman logo
function drawThrobber(num) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(253,227,17);
  ellipse(35,0,22,22);
  pop();

}


//this defines that if the video is not playing and the mouse is pressed, then the video will begin to play
function mousePressed() {
  if (!playing) {
    batman.play();
        playing = true;
      }
    }

    //this defines that when the mouse button is no longer pressed the video will pause
function mouseReleased() {
  if (playing) {
    batman.pause();
    playing = false;
  } }
